import sys
sys.path.append('./')
from QuaternionFourier import *
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
from skimage.transform import rescale, resize, downscale_local_mean

path = "storagetanks01.jpeg"


im = Image.open(path)
image = np.array(im)
image = resize(image, (image.shape[0] // 4, image.shape[1] // 4),anti_aliasing=True)
#spectrum = ForwardQuaternionTransformType1(image)
transform = QuaternionFourierTransform(image)
spectrum = transform.ForwardTransform(type = 3)

#for index in range(4):
#   plt.figure()
#   plt.imshow(spectrum[...,index] / spectrum[...,index].max())

plt.figure()
plt.imshow(image)
reverse =transform.InverseTransform()#InverseQuaternionTransformType1(spectrum)#
#reverse = InverseQuaternionTransformType1(spectrum)
#reverse[...,1] = -reverse[...,1]
#reverse[...,2] = -reverse[...,2]
plt.figure()
plt.imshow(reverse)
