import numpy as np
import scipy.fftpack as fftStuff

'''
In this case mu1 = i, mu2 = j, mu3 = k
'''

class QuaternionFourierTransform():
    def __init__(self, image):
        self.numRow = image.shape[0]
        self.numCol = image.shape[1]
        self.numBand = image.shape[2]
        self.originalImage = image
        self.spectrum = np.zeros((self.numRow,self.numCol)+ (4,))
        self.reconstructedImage = np.zeros(image.shape)

        self.type = 1

    def __ForwardType1(self, part1, part2):
        spectrum = np.zeros((self.numRow,self.numCol)+ (4,))

        spectrum1 = fftStuff.fft2(part1)
        spectrum1 = fftStuff.fftshift(spectrum1)

        spectrum2 = fftStuff.fft2(np.transpose(part2, axes=(0,1)))
        spectrum2 = fftStuff.fftshift(spectrum2)

        spectrum3 = fftStuff.fft2(np.transpose(part1, axes=(0,1)))
        spectrum3 = fftStuff.fftshift(spectrum3)

        spectrum4 = fftStuff.fft2(part2)
        spectrum4 = fftStuff.fftshift(spectrum4)

        spectrum[:,:, 0] = spectrum1.real + spectrum2.imag + spectrum3.real - spectrum4.imag
        spectrum[:,:, 1] = spectrum1.imag - spectrum2.real + spectrum3.imag + spectrum4.real
        spectrum[:,:, 2] = spectrum1.imag + spectrum2.real - spectrum3.imag + spectrum4.real
        spectrum[:,:, 3] = -spectrum1.real + spectrum2.imag + spectrum3.real + spectrum4.imag

        self.spectrum = 0.5 * spectrum

        return self.spectrum

    def __ForwardType2(self, part1, part2):
        spectrum = np.zeros((self.numRow,self.numCol)+ (4,))

        spectrum1 = fftStuff.fft2(part1)
        spectrum1 = fftStuff.fftshift(spectrum1)

        spectrum2 = fftStuff.fft2(part2)
        spectrum2 = fftStuff.fftshift(spectrum2)

        spectrum[:,:, 0] = spectrum1.real
        spectrum[:,:, 1] = spectrum1.imag
        spectrum[:,:, 2] = spectrum2.real
        spectrum[:,:, 3] = spectrum2.imag

        self.spectrum = spectrum

        return self.spectrum

    def __ForwardType3(self, part1, part2):    
        spectrum = np.zeros((self.numRow,self.numCol)+ (4,))

        spectrum1 = fftStuff.fft2(part1)
        spectrum1 = fftStuff.fftshift(spectrum1)

        spectrum2 = fftStuff.ifft2(part2)
        spectrum2 = fftStuff.fftshift(spectrum2)

        spectrum[:,:, 0] = spectrum1.real
        spectrum[:,:, 1] = spectrum1.imag
        spectrum[:,:, 2] = spectrum2.real
        spectrum[:,:, 3] = spectrum2.imag

        self.spectrum = spectrum

        return self.spectrum

    def ForwardTransform(self, type = 1):
        self.type = type
        self.spectrum = np.zeros((self.numRow,self.numCol)+ (4,))
        real_part = np.zeros((self.originalImage[:,:,0].shape))
        part1 = real_part + 1j *self.originalImage[:,:,0]
        part2 = self.originalImage[...,1] + 1j * self.originalImage[...,2]

        if self.type == 1:
            return self.__ForwardType1(part1, part2)
        elif self.type == 2:
            return self.__ForwardType2(part1, part2)
        elif self.type == 3:
            return self.__ForwardType3(part1, part2)
        else:
            return self.__ForwardType1(part1, part2)

    def __InverseType1(self, part1, part2):
        image1 = fftStuff.ifftshift(part1)
        image1 = fftStuff.ifft2(image1)

        image2 = np.transpose(fftStuff.ifftshift(part2), axes=(0,1))
        image2 = fftStuff.ifft2(image2)

        image3 = np.transpose(fftStuff.ifftshift(part1), axes=(0,1))
        image3 = fftStuff.ifft2(image3)

        image4 = fftStuff.ifftshift(part2)
        image4 = fftStuff.ifft2(image4)


        tmpImage = np.zeros(self.spectrum.shape)
        tmpImage[:,:, 0] = image1.real + image2.imag + image3.real - image4.imag
        tmpImage[:,:, 1] = image1.imag - image2.real + image3.imag + image4.real
        tmpImage[:,:, 2] = image1.imag + image2.real - image3.imag + image4.real
        tmpImage[:,:, 3] = -image1.real + image2.imag + image3.real + image4.imag

        tmpImage = 0.5 * tmpImage
        
        self.reconstructedImage[:,:, 0] = tmpImage[:,:, 1]
        self.reconstructedImage[:,:, 1] = tmpImage[:,:, 2]
        self.reconstructedImage[:,:, 2] = tmpImage[:,:, 3]

        return self.reconstructedImage 

    def __InverseType2(self, part1, part2):
        image1 = fftStuff.ifftshift(part1)
        image1 = fftStuff.ifft2(image1)

        image2 = fftStuff.ifftshift(part2)
        image2 = fftStuff.ifft2(image2)

        tmpImage = np.zeros(self.spectrum.shape)
        tmpImage[:,:, 0] = image1.real
        tmpImage[:,:, 1] = image1.imag
        tmpImage[:,:, 2] = image2.real
        tmpImage[:,:, 3] = image2.imag

        self.reconstructedImage[:,:, 0] = tmpImage[:,:, 1]
        self.reconstructedImage[:,:, 1] = tmpImage[:,:, 2]
        self.reconstructedImage[:,:, 2] = tmpImage[:,:, 3]

        return self.reconstructedImage

    def __InverseType3(self, part1, part2):
        image1 = fftStuff.ifftshift(part1)
        image1 = fftStuff.ifft2(image1)

        image2 = fftStuff.ifftshift(part2)
        image2 = fftStuff.fft2(image2)

        tmpImage = np.zeros(self.spectrum.shape)
        tmpImage[:,:, 0] = image1.real
        tmpImage[:,:, 1] = image1.imag
        tmpImage[:,:, 2] = image2.real
        tmpImage[:,:, 3] = image2.imag

        self.reconstructedImage[:,:, 0] = tmpImage[:,:, 1]
        self.reconstructedImage[:,:, 1] = tmpImage[:,:, 2]
        self.reconstructedImage[:,:, 2] = tmpImage[:,:, 3]

        return self.reconstructedImage
        
    def InverseTransform(self) :
        self.reconstructedImage = np.zeros(self.originalImage.shape)
        part1 = self.spectrum[:,:,0] + 1j * self.spectrum[:,:,1]
        part2 = self.spectrum[:,:,2] + 1j * self.spectrum[:,:,3]

        if self.type == 1:
            return self.__InverseType1(part1, part2)
        elif self.type == 2:
            return self.__InverseType2(part1, part2)
        elif self.type == 3:
            return self.__InverseType3(part1, part2)
        else:
            return self.__InverseType1(part1, part2)